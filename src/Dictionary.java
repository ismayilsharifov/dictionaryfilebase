import java.io.*;
import java.util.*;

public class Dictionary {
    public static void main(String[] args) {
        Map<String,String>AzEnMap = new HashMap<>();
        Map<String,String>EnAzMap = new HashMap<>();

        AzEnMap.put("luget","dictionary");
        EnAzMap.put("dictionary","luget");

        System.out.println("\t\t\t\t\nWords stored in local files.");
        int count = 0;
        do {
            if(count==0) {
                writeFileWriterAZ("test");
                writeFileWriterEN("test");
            }
            List<String> ENG = readFileReaderEn();
            List<String> AZE = readFileReaderAz();
            Scanner in = new Scanner(System.in);
            System.out.println("\nEnter a word...");
            String s = in.next();

            if (AzEnMap.containsKey(s)) {
                System.out.println(AzEnMap.get(s));
                if(!AZE.contains(s)) {
                    writeFileWriterAZ(s);
                    writeFileWriterEN(AzEnMap.get(s));
                }
            } else if (EnAzMap.containsKey(s)) {
                System.out.println(EnAzMap.get(s));
                if(!ENG.contains(s)) {
                    writeFileWriterEN(s);
                    writeFileWriterAZ(EnAzMap.get(s));
                }
            } else {
                if (AZE.contains(s)) {
                    int index = AZE.indexOf(s);
                    System.out.println(ENG.get(index));
                } else if (ENG.contains(s)) {
                    int index = ENG.indexOf(s);
                    System.out.println(AZE.get(index));
                } else {
                    System.out.print("Not found.Are you want to add it? (y/n)");
                    char ch = in.next().charAt(0);
                    if (ch == 'y') {
                        System.out.print("Which language? (Az/En)");
                        String str = in.next();
                        if (str.toLowerCase().equals("az")) {
                            System.out.println("Please enter the alternativ (en) word for translate. ");
                            String s1 = in.next();
                            AzEnMap.put(s, s1);
                            if (!AZE.contains(s)) {
                                writeFileWriterAZ(s);
                                writeFileWriterEN(s1);
                            }
                        } else if (str.toLowerCase().equals("en")) {
                            System.out.println("Please enter the alternativ (az) word for translate. ");
                            String s1 = in.next();
                            EnAzMap.put(s, s1);
                            if (!ENG.contains(s)) {
                                writeFileWriterEN(s);
                                writeFileWriterAZ(s1);
                            }
                        }
                    } else {
                        System.out.println("Sozleri elave etmedikce lugetimiz zenginleshmir!");
                    }
                }
            }
            System.out.print("\nAre you want continue? (y/n) ");
            char c = in.next().charAt(0);
            if (c != 'y') {
                System.out.println("\n\t\t\t\tThanks for using!");
                break;
            }
            count++;
        }while(true);
    }
    private static void writeFileWriterAZ(String data){
        File file1 = new File("AzWords.txt");
        FileWriter azWriter = null;
        BufferedWriter az = null;
        String dataNewLine = data + System.getProperty("line.separator");
        try{
            azWriter = new FileWriter(file1,true);
            az = new BufferedWriter(azWriter);
            az.write(dataNewLine);
        }catch (IOException e){
            e.printStackTrace();
            e.getMessage();
        }finally {
            try{
                az.close();
                azWriter.close();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private static void writeFileWriterEN(String data){
        File file2 = new File("EnWords.txt");
        FileWriter enWriter = null;
        BufferedWriter en = null;
        String dataNewLine = data + System.getProperty("line.separator");
        try{
            enWriter = new FileWriter(file2,true);
            en = new BufferedWriter(enWriter);
            en.write(dataNewLine);
        }catch (IOException e){
            e.printStackTrace();
            e.getMessage();
        }finally {
            try{
                en.close();
                enWriter.close();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private static List<String> readFileReaderAz(){
        File file1 = new File("AzWords.txt");
        FileReader azReader=null;
        BufferedReader az = null;
        String line;
        ArrayList<String> words = new ArrayList<>();
        try {
            azReader = new FileReader(file1);
            az = new BufferedReader(azReader);
            while((line = az.readLine())!=null){
                words.add(line);
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return words;
    }
    private static List<String> readFileReaderEn(){
        File file1 = new File("EnWords.txt");
        FileReader enReader=null;
        BufferedReader en = null;
        String line;
        ArrayList<String> words = new ArrayList<>();
        try {
            enReader = new FileReader(file1);
            en = new BufferedReader(enReader);
            while((line = en.readLine())!=null){
                words.add(line);
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return words;
    }
}
